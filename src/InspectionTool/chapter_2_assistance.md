# Assistance
## Auto sensory panel
<span style="color:red">
These results reflect association of vocs with aryballe sensors. These findings might or might not reflect human perception of smell. The goal of this tool is to assist human panel.</span>


This activity is inspired by Human-panel form that mainly contains:
1. Intensity: revel the information about intensity.
    SAK provide comparative information about intensity. Intensity of each voc is plotted as bar chart with error bar.  


![intensity](../Images/Assistance/AHP_intensity.png)




2. ~~Headonic~~ (Signature Similarity): Human panel gives information related to hedonic. In order to provide relevant information SAK provide signature similarity using simple distance metric. 
![similarity](../Images/Assistance/AHP_similarity.png)
