# PCA 
This is default activity of sak-Inspection tools. This page display following plots:
1. Eigen signatures
2. User define 2D sensor plot.
3. PCA plots:
    - PC 1 vs PC 2
    - PC 2 vs PC 3
    - PC 3 vs PC 4
### Example

**SAK-Token used in this tutorial**:
SAK-03c0e545-4dea-440a-bb46-9bb39ef228eb

### EigenSignatures
EigenSignatures represent the contribution of principal components. These signatures could be used to identify peptide involve in discriminating different VOCs. For example, in the figure below EigenSignature-1 shows *spot254* has major contribution along first principal axis. Similarily, a linear combination of *spot294*, *spot194*, *spot554*, *spot264* and others are major contributor along PC2. 
![eigenSignature](../Images/PCA/EigenSignaturesInspectiontool.png)

### PCA plots
Inspection tool by default plots:  
* PC 1 vs PC 2  
* PC 2 vs PC 3  
* PC 3 vs PC 4  

![pca](../Images/PCA/PC1vsPC2.png)

Each figure is annotated with
* Title: **PCA CQS in percentage** (CQS is computed using PCs shown in a figure). For example, CQS of figure PC 2 vs PC 3  is computed using PC 2 vs PC 3 and this computation **does not include** other PCs (like PC 1). 
* X and Y axis: PC (Explained variance in %) and Singular Value.

* Circle markers: all circles are annotated with cycle number. 

### Sensors 2D plot

A user can select 2 spots to display the 2D plot. CQS is computed based upon these two sensors. 