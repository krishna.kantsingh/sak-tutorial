# Experimental conditions

This activity is devoted for plotting experimental conditions associated with an experiment.

Following temporal and distribution plots of humidity and temperature can be obtain in this activity:

1. Baseline Humidity
2. Baseline Humidity
3. Delta temperature and Humidity

![ExperimentalProperties](../Images/ExperimentalProperties/HTExp.png)
