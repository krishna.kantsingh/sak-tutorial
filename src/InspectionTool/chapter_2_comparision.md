# Comparision 

As name suggested, this activity is used to compare different signatures using:
1. Hierarchial Clustering
2. Euclidean Distance Matrix 
3. Pair comparision of two signatures
    * Distance between two signatures
    * t-test with Bonferroni corrected p value  
    * Comparision of distribution of two VOCs using distribution plot. 

### Hierarchial Clustering

This plot could be used to identify similarity between different signatures.  
**Distances are multiplied by 100.**  
![clusteringwithDistance](../Images/Comparision/distance.png)


### Compare Signatures 
Plots only two signatures for display. Moreover, Manhattan and euclidean distances are provided for further evaluation. 

![clusteringwithDistance](../Images/Comparision/comparision2Signatures.png)


We also provide t-test per peptide (use with caution).

![clusteringwithDistance](../Images/Comparision/pvalueswithDistribution.png)