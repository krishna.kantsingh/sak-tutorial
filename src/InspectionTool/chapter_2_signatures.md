# Visualization (Signatures)

This activity is used to inspect signatures with following plots:
1. Signature plots:
    * Raw Signatures
    * Normalized signatures
2. Intensity plots:
    * Maximum Intensity
    * Median Intensity
    * Intensity Contrast = (MaxI - MinI)/MaxI 

All intensity based plot has temporal (on left) with x axis as cycles and distribution plot (on right). 
![maxintensity](../Images/signatures/signaturesMaxIntensity.png)
