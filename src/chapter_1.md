# SAK-IO (Input-Output) Tutorial
<!-- ## SAK-IO  -->
## Access 
IO module of sak can be access by any clicking theese links:  
[sak-io:DNS](http://sak-01.dev.ary:8501/ "DNS")  
[sak-io:IP](http://10.5.1.158:8501/ "IP")

**TL;DR**

![SAKIO](TLDR-SAK-io.png)

**Things to consider**  
For sqlite data source: Path must start as *"\\\Srv-shares\shares"*

**Example Paths**:  
[1] NOA data:   


"\\\Srv-shares\shares\Databases\211117_sauvegarde Labotest1-1\set3.sqlite"  

[2] Amplifier data:    

"\\\Srv-shares\shares\Databases\211115_amplifier_repro\211116_5_devices_amplifier_J1\211116_fsp-fnf-solution_BELGIQUE_amplifier_J1.sqlite"


--------------------------------
